Source: python-os-brick
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 James Page <james.page@ubuntu.com>,
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr (>= 5.8.0),
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-babel,
 python3-castellan (>= 3.10.0),
 python3-coverage (>= 5.1),
 python3-ddt,
 python3-eventlet (>= 0.30.1),
 python3-hacking,
 python3-openstackdocstheme <!nodoc>,
 python3-os-win (>= 5.5.0),
 python3-oslo.concurrency (>= 4.5.0),
 python3-oslo.config,
 python3-oslo.context (>= 3.4.0),
 python3-oslo.i18n (>= 5.1.0),
 python3-oslo.log (>= 4.6.1),
 python3-oslo.privsep (>= 2.6.2),
 python3-oslo.serialization (>= 4.2.0),
 python3-oslo.service (>= 2.8.0),
 python3-oslo.utils (>= 4.12.1),
 python3-oslo.vmware (>= 3.10.0),
 python3-oslotest (>= 1:4.5.0) <!nocheck>,
 python3-reno,
 python3-requests,
 python3-retrying,
 python3-stestr (>= 3.2.1) <!nocheck>,
 python3-tenacity (>= 6.3.1),
 python3-testscenarios <!nocheck>,
 python3-testtools <!nocheck>,
 subunit,
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/openstack-team/libs/python-os-brick
Vcs-Git: https://salsa.debian.org/openstack-team/libs/python-os-brick.git
Homepage: https://github.com/openstack/os-brick

Package: os-brick-common
Architecture: all
Depends:
 ${misc:Depends},
Description: Library for managing local volume attaches - common files
 OpenStack Cinder brick library for managing local volume attaches.
 .
 Features discovery of volumes being attached to a host for many
 transport protocols and removal of volumes from a host.
 .
 This package contains common files needed by either the Python 2 or 3 module
 of os-brick.

Package: python-os-brick-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: Library for managing local volume attaches - doc
 OpenStack Cinder brick library for managing local volume attaches.
 .
 Features discovery of volumes being attached to a host for many
 transport protocols and removal of volumes from a host.
 .
 This package contains the documentation.

Package: python3-os-brick
Architecture: all
Depends:
 open-iscsi,
 os-brick-common (= ${binary:Version}),
 python3-babel,
 python3-eventlet (>= 0.30.1),
 python3-os-win (>= 5.5.0),
 python3-oslo.concurrency (>= 4.5.0),
 python3-oslo.config,
 python3-oslo.context (>= 3.4.0),
 python3-oslo.i18n (>= 5.1.0),
 python3-oslo.log (>= 4.6.1),
 python3-oslo.privsep (>= 2.6.2),
 python3-oslo.serialization (>= 4.2.0),
 python3-oslo.service (>= 2.8.0),
 python3-oslo.utils (>= 4.12.1),
 python3-pbr (>= 5.8.0),
 python3-requests,
 python3-retrying,
 python3-tenacity (>= 6.3.1),
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-os-brick-doc,
Description: Library for managing local volume attaches - Python 3.x
 OpenStack Cinder brick library for managing local volume attaches.
 .
 Features discovery of volumes being attached to a host for many
 transport protocols and removal of volumes from a host.
 .
 This package contains the Python 3.x module.
